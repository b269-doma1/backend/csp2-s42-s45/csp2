

const Product = require("../models/Product");



// Add product
module.exports.addProduct = (data) => {
	
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		return newProduct.save().then((product, error) => {
			
			if (error) {
				return false;
			
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// get active product
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};


// retrieve single products
module.exports.getProduct = (reqParams)=> {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
	};


	return Product.findByIdAndUpdate(reqParams.courseId, updatedProduct).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


// Archive  product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};


	return Product.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true
		};
	});
};


