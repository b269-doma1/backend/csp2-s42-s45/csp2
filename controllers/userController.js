

const User = require("../models/User");
const Course = require("../models/Product");


const bcrypt = require("bcrypt");

const auth = require("../auth");


// check user exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		};
	});

};


// register
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return	true;
		}
	})
};


// authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		
		if(result == null){
			return false
			
		} else {
			isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				
			} else {
				return false;
			}
		}
	})
};

