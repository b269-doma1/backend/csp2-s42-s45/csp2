
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");

const productRoute = require("./routes/productRoute");


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);

app.use("/product", productRoute);



mongoose.connect("mongodb+srv://richrddoma:admin123@zuitt-bootcamp.xpydmcq.mongodb.net/Capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log('Now conected to cloud database!'));

app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));