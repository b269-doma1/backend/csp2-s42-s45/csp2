

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email : {
		type: String,
		required : [true]
	},

	password : {
		type: String,
		required : [true]
	},

	isAdmin : {
		type: Boolean,
		default : false
	},

	orders : [{

		products : [{
			productName: {
				type : String,
				required : [true]
			},
				quantity: {
				type : Number,
				required : [true]
			}
		}],

		totalAmount : {
			type: Number,
			required : [true]
		},

		purchasedOn : {
			type : Date,
			default : new Date()
		}

	}]

});

module.exports = mongoose.model("User", userSchema);