

const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");





// add product
router.post("/add", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// retrieve active product
router.get("/active", (req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));

});



// update product
router.put("/:courseId", (req,res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});




module.exports = router;